[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://raw.githubusercontent.com/kherbiche/hiempsal/master/LICENSE)

# hiempsal

## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Running the App](#running-the-app)
4. [License](#License)
### General Info
***
These instructions will get you a copy of the project up and running on your local machine. Enjoy.

## Technologies
***
You need to install the softwares like:
* [Java 8 or higher](https://www.oracle.com/fr/java/technologies/javase/javase8-archive-downloads.html): Version 8
* [Apache Maven](https://maven.apache.org/download.cgi): Version

## Running the App
Download & unzip or clone this project
```
git clone https://gitlab.com/kherbiche/hiempsal
```
With Terminal or cmd, navigate to the root of the project cloned or unziped then run:
```
start.sh or start.bat then callws.sh
```
-The App runs on localhost:8088/api/users/

-H2 Data Base Console on http://localhost:8088/console , with jdbc url: jdbc:h2:mem:hiempsaldb, login: sa

-OpenAPI Doc on http://localhost:8088/swagger-ui/index.html

## License
[gpl](https://www.gnu.org/licenses/gpl-3.0.html)
